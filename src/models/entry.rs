use super::feed::Feed;
use crate::{EntryID, FeedID, UserID};
use serde_derive::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize)]
pub struct Entry {
    pub id: EntryID,
    pub user_id: UserID,
    pub feed_id: FeedID,
    pub title: String,
    pub url: String,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub comments_url: Option<String>,
    pub author: String,
    pub content: String,
    pub hash: String,
    pub published_at: String,
    pub status: String,
    pub starred: bool,
    pub feed: Feed,
}

impl Entry {
    /// destroy this entry and gain ownership of all the data it contains
    #[allow(clippy::type_complexity)]
    pub fn decompose(
        self,
    ) -> (
        EntryID,
        UserID,
        FeedID,
        String,
        String,
        Option<String>,
        String,
        String,
        String,
        String,
        String,
        bool,
        Feed,
    ) {
        (
            self.id,
            self.user_id,
            self.feed_id,
            self.title,
            self.url,
            self.comments_url,
            self.author,
            self.content,
            self.hash,
            self.published_at,
            self.status,
            self.starred,
            self.feed,
        )
    }
}

#[derive(Debug, Serialize)]
pub struct EntryStateUpdate {
    pub entry_ids: Vec<EntryID>,
    pub status: String,
}

#[derive(Debug, Deserialize)]
pub struct EntryBatch {
    pub total: i64,
    pub entries: Vec<Entry>,
}

impl EntryBatch {
    /// destroy this entry batch and gain ownership of all the entries it contains
    pub fn decompose(self) -> (i64, Vec<Entry>) {
        (self.total, self.entries)
    }
}
